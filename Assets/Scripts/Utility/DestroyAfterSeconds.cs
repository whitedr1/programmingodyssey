using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterSeconds : MonoBehaviour
{
    public float seconds;

    private void Awake()
    {
        // Destroy this GameObject after a number of seconds
        Destroy(gameObject, seconds);
    }
}
