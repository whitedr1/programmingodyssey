using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Commlink : MonoBehaviour
{
    // Singleton reference
    private static Commlink instance;

    public enum Planets
    {
        Mercury,
        Venus,
        Earth,
        Mars,
        Jupiter,
        Saturn,
        Uranus,
        Neptune
    }

    // I feel like we're missing something here...

    [Tooltip("The current health of the ship")]
    [Range(0, 100)]
    [SerializeField] private int currentHealth = 100;

    [Tooltip("The list of crew on-board")]
    [SerializeField] private List<ScriptableCrew> crewMembers;

    private void Awake()
    {
        // Why is this empty?
    }

    private void Start()
    {
        BroadcastData();
    }

    /// <summary>
    /// Log all data for Earth to recieve
    /// </summary>
    public void BroadcastData()
    {
        // Where's our debugs?
        
        foreach (ScriptableCrew member in crewMembers)
        {
            Debug.Log($"{member.name}: {member.crewName}");
        }
    }
}
