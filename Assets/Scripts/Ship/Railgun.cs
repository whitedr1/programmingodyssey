using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Railgun : MonoBehaviour
{

    [Header("Railgun Controls")]
    [SerializeField] private float laserTime = 0.5f;

    // Firing values
    private bool isFired = false;
    private Vector3 offset = Vector3.forward * 2;

    // References
    private LineRenderer rend;
    private Hyperdrive drive;

    private void Start()
    {
        // Hmmm

        // Turn off laser renderer initially
        rend.enabled = false;
    }

    private void Update()
    {
        // If left click, fire railgun!
        if (Input.GetMouseButtonDown(0))
        {
            if (!isFired)
            {
                isFired = true;
                drive.ToggleStationary(true);
                // Wait a minute...
            }
        }
    }

    /// <summary>
    /// Handles the firing sequence and raycast checking
    /// </summary>
    /// <returns>Null time</returns>
    IEnumerator FireRailgun()
    {
        RaycastHit hit;
        Vector3 target = Vector3.zero;

        Debug.DrawRay(transform.position, transform.forward * 1000, Color.green);

        // Send out a raycast forward, check if hit a surface
        // Wait!
        if (Physics.Raycast(transform.position, transform.right, out hit, 0))
        {
            target = hit.point;
        }
        else
        {
            isFired = false;
            drive.ToggleStationary(false);
            yield break;
        }

        // If we've hit, turn renderer on and set it's points based on raycast
        float currTime = 0;
        rend.enabled = true;

        while (currTime < laserTime)
        {
            rend.SetPosition(0, this.transform.position);
            rend.SetPosition(1, target);

            currTime += Time.deltaTime;

            yield return null;
        }

        rend.enabled = false;
        drive.ToggleStationary(false);
        isFired = false;
    }
}
