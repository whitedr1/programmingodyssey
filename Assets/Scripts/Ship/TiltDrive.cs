using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiltDrive : MonoBehaviour
{
    public float tiltSpeed;
    public float maxHoriTilt;
    public float maxVertTilt;

    public GameObject shipModel;

    private Vector3 startingRotation;

    private void Start()
    {
        startingRotation = shipModel.transform.localRotation.eulerAngles;
    }

    private void Update()
    {
        // Get Input
        float horiInput = Input.GetAxis("Horizontal");
        float vertInput = Input.GetAxis("Vertical");

        // Keep track of rotation
        float currentHoriRotation = shipModel.transform.localRotation.eulerAngles.y;
        float currentVertRotation = shipModel.transform.localRotation.eulerAngles.z;

        // Calculate horizontal rotation
        if (horiInput != 0)
        {
            float targetHoriRotation = startingRotation.y + maxHoriTilt * -horiInput;

            currentHoriRotation = Mathf.LerpAngle(currentHoriRotation, targetHoriRotation, tiltSpeed * Time.deltaTime);
        }

        // Calculate vertical rotation
        if (vertInput != 0)
        {
            float targetVertRotation = startingRotation.z + maxVertTilt * vertInput;

            currentVertRotation = Mathf.LerpAngle(currentVertRotation, targetVertRotation, tiltSpeed * Time.deltaTime);
        }

        // Apply rotation
        shipModel.transform.localRotation = Quaternion.Euler(new Vector3(0, currentHoriRotation, currentVertRotation));
    }
}
