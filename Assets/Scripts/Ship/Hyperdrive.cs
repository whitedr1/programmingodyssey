using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hyperdrive : MonoBehaviour
{

    [Header("Ship Controls")]

    [Tooltip("The turn speed of the ship's rotational movement")]
    [SerializeField] private float turnSpeed = 60f;

    [Tooltip("The maximum speed of the ship")]
    [SerializeField] private float maximumSpeed = 45f;

    [Tooltip("The acceleration of the ship's drive")]
    [SerializeField] private float acceleration = 0.5f;

    [Tooltip("The acceleration of the ship's drive")]
    [SerializeField] private float deAcceleration = 2f;

    [Tooltip("The time needed to rebalance")]
    [SerializeField] private float balanceTime = 3;

    // Rebalancing values
    private float currentSpeed = 0;
    private bool stationary = false;

    // References
    private Rigidbody body;

    private void Start()
    {
        body = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        // BRAKING
        if (Input.GetKey(KeyCode.LeftShift))
        {
            // What happened here?
        }
    }

    private void FixedUpdate()
    {

    }

    /// <summary>
    /// Handles the rotational turning based on player axis input
    /// </summary>
    void Turn()
    {
        // Something about this isn't right...
        float yaw = turnSpeed * Input.GetAxis("Horizontal");
        float pitch = turnSpeed * Input.GetAxis("Vertical");
        float roll = turnSpeed * Input.GetAxis("Rotate");

        if (!stationary)
        {
            transform.Rotate(pitch, yaw, roll);
        }
    }

    /// <summary>
    /// Adds speed to the rigidbody (forward movement)
    /// </summary>
    void Thrust()
    {

        if (!stationary)
        {
            body.velocity = transform.forward * maximumSpeed;
        }
    }

    public void ToggleStationary(bool isStationary)
    {
        stationary = isStationary;
    }
}
