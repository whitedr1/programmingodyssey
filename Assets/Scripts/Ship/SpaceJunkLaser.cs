using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceJunkLaser : MonoBehaviour
{
    public float fireCooldown;
    public Vector3 bulletSpawnOffset;
    public GameObject bulletPrefab;

    private float timeSinceLastFire = 0f;
    public List<GameObject> junkInRange = new List<GameObject>();

    private void Update()
    {
        // Can we fire yet?
        timeSinceLastFire += Time.deltaTime;
        if (junkInRange.Count != 0 && timeSinceLastFire >= fireCooldown)
        {
            Fire();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // Add Junk to list
        if (other.CompareTag("Junk"))
        {
            junkInRange.Add(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        // Remove Junk from list
        if (other.CompareTag("Junk"))
        {
            junkInRange.Remove(other.gameObject);
        }
    }

    private void Fire()
    {
        // Find the closest piece of junk
        GameObject closest = null;
        float closestDist = float.MaxValue;
        foreach (GameObject junk in junkInRange)
        {
            float junkDist = Vector3.Distance(transform.position, junk.transform.position);

            if (junkDist < closestDist)
            {
                closest = junk;
                closestDist = junkDist;
            }
        }

        // Fire at the closest piece of junk
        GameObject bullet = Instantiate<GameObject>(bulletPrefab);
        bullet.transform.position = transform.position + bulletSpawnOffset;
        bullet.transform.LookAt(closest.transform, Vector3.up);

        // Remove the junk from the list
        junkInRange.Remove(closest);

        // Reset cooldown
        timeSinceLastFire = 0f;
    }
}
