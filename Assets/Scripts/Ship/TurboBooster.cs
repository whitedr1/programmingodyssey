using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurboBooster : MonoBehaviour
{

    public float boostSpeed = 100;
    public float boostTime = 3;
    public float boostCooldown = 1;

    private bool boosting = false;

    // References
    private Hyperdrive drive;
    private Rigidbody body;

    private void Start()
    {
        drive = GetComponentInParent<Hyperdrive>();
        body = GetComponentInParent<Rigidbody>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            boosting = true;
            drive.ToggleStationary(true);
            StartCoroutine("BoostSequence");
        }
    }

    IEnumerator BoostSequence()
    {
        Vector3 initVelocity = body.velocity;

        body.velocity = transform.forward * boostSpeed;

        yield return new WaitForSeconds(boostTime);

        // Huh?
        int currTime = 0;

        while (currTime < boostCooldown)
        {
            body.velocity = Vector3.Lerp(body.velocity, initVelocity, currTime / boostCooldown);
            body.angularVelocity = Vector3.Lerp(body.angularVelocity, Vector3.zero, currTime / boostCooldown);

            // Wait...

            yield return null;
        }

        drive.ToggleStationary(false);
        boosting = false;
    }
}
