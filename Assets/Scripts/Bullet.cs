using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed;

    private void Update()
    {
        // Move this Bullet
        transform.position += transform.forward * speed * Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        // If this hits a piece of junk, destroy the junk
        if (other.CompareTag("Junk"))
        {
            Destroy(other.gameObject);
        }
    }
}
