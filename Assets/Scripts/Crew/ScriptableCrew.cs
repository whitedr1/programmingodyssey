using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ScriptableCrew : ScriptableObject
{
    public string crewName;
    [SerializeField] private Color favoriteColor;
}
